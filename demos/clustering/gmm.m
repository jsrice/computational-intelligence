%{
Gaussian Mixture Model (GMM) demo
Course:       Computational Intelligence (Fall 2015)
Instructor:   Dr. Timothy Havens
Institution:  Michigan Technological University
Language:     MATLAB
%}

n=200;
R1=chol([1 .5; .5 10]);
R2=chol([1 0; 0 10]);
R3=chol([8 -2; -2 8]);

X=[randn(n,2)*R1
    bsxfun(@plus,randn(n,2)*R2,[5 4])
    bsxfun(@plus,randn(n,2)*R3,[-5 0 ])];
class = [ones(n,1)
    2*ones(n,1)
    3*ones(n,1)];

c=3;

[n,d]=size(X);

init=randperm(n);
for j=1:c,
    Gmodel.mu{j} = X(init(j),:);
    Gmodel.S{j} = eye(d);
    Gmodel.pi{j} = 1/c;
end;

tmax = 1000; eps=1e-3;
t=0; gamma=ones(n,c); gammap=0;
while(max(abs(gamma(:)-gammap))>eps & t<tmax)
    gammap=gamma(:);
    % E-step
    for j=1:c,
        dum = bsxfun(@minus,X,Gmodel.mu{j});
        dum2 = det(Gmodel.S{j})^(-0.5);
        gamma(:,j) = Gmodel.pi{j}*dum2*diag(exp(-0.5*dum*pinv(Gmodel.S{j})*dum'));
    end;
    gamma = bsxfun(@rdivide,gamma,sum(gamma,2));

    [dum,i]=max(gamma,[],2);
    figure(1); clf; hold all;
    for j=1:c,
        plot(X(i==j,1),X(i==j,2),'.');
        plotcov2(Gmodel.mu{j}(1:2),Gmodel.S{j}(1:2,1:2),'plot-opts',{'Color','k'});
    end;
    axis equal;
    waitforbuttonpress;

    for j=1:c,
        Ng = sum(gamma(:,j));
        Gmodel.pi{j}=Ng/n;

        dum = bsxfun(@minus,X,Gmodel.mu{j});
        S=zeros(d);
        for i=1:n,
            S = S + gamma(i,j)*dum(i,:)'*dum(i,:);
        end;
        Gmodel.S{j}=S/Ng;

        Gmodel.mu{j}=sum(bsxfun(@times,X,gamma(:,j)))/Ng;
    end;
end;


% [dum,idx]=max(gamma,[],2);
% Uhard = full(sparse(1:n,idx,1));
% Uclass = full(sparse(1:n,class,1));
% ComparingPartitions(Uhard',Uclass')


