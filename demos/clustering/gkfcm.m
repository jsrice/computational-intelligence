%{
Gaussian Kernel-based Fuzzy C-Means (GKFCM) demo
Course:       Computational Intelligence (Fall 2015)
Instructor:   Dr. Timothy Havens
Institution:  Michigan Technological University
Language:     MATLAB
%}

clear all; close all;

n=200;
R1=chol([1 .5; .5 2]);
R2=chol([1 0; 0 10]);
R3=chol([1 -2; -2 8]);

mx = 10;
X=[randn(n,2)*R1
    bsxfun(@plus,randn(n,2)*R2,[5 4])
    bsxfun(@plus,randn(n,2)*R3,[-5 0 ])];
class = [ones(n,1)
    2*ones(n,1)
    3*ones(n,1)];

c=3;

% X=[-1 1
%     -1 0
%     -1 -1
%     0 0
%     1 1
%     1 0
%     1 -1];
% c=2;


figure(1);
plot(X(:,1),X(:,2),'.');

[N,d]=size(X);

m=1.7;
p=1;



init=randperm(N);
V=X(init(1:c),:);


plot(X(:,1),X(:,2),'b.',V(:,1),V(:,2),'rx'); waitforbuttonpress;

m2=1/(m-1);
tmax=100;
eps=1e-3;
Vold=zeros(c,d);
for k=1:c,
    A{k}=eye(d);
end;

[XX YY]=meshgrid(-12:.1:15,-10:.1:20);
%figure; hold on;
t=1;
D=zeros(N,c);
while(any(sum((V-Vold).^2,2)>eps) && t<tmax)
    t
    Vold=V;
    for k=1:c,
        D(:,k)=distance2(X,V(k,:),A{k});
    end;
    D(D<0)=0; D=D.^m2;
    U=1./bsxfun(@times,D,sum(1./D,2));
    U(isnan(U))=1;

    Um=U.^m;

    for k=1:c,
        V(k,:)=sum(bsxfun(@times,X,Um(:,k)))/sum(Um(:,k));
        Anew=zeros(d);
        for i=1:N,
            XV=X(i,:)-V(k,:);
            Anew=Anew+Um(i,k)*XV'*XV;
        end;
        A{k}=det(Anew).^(1/d)*inv(Anew);
    end;
    plot(X(:,1),X(:,2),'b.',V(:,1),V(:,2),'rx');
    hold on;
    for k=1:c,
        obj=gmdistribution(V(k,:),inv(A{k}),1);
        Z=reshape(pdf(obj,[XX(:) YY(:)]),size(XX));
        contour(XX,YY,Z);
    end;
    hold off; axis equal; waitforbuttonpress;

    t=t+1;
    %plot(t,V(:,1),'.');
end;
%t

D=distance2(X,V); D(D<0)=0; D=D.^m2;
U=1./bsxfun(@times,D,sum(1./D,2));
U(isnan(U))=1;

[x,y]=meshgrid(-mx:.01:mx);
Xm = [x(:) y(:)];

D = distance2(Xm,V); D(D<0)=0; D=D.^m2;
Um = 1./bsxfun(@times,D,sum(1./D,2));
Um(isnan(Um)) = 1;

figure;
for k = 1:c,
    clf;
    plot(X(:,1),X(:,2),'.',V(:,1),V(:,2),'x'); hold on;
    contour(x,y,reshape(Um(:,k),size(x)),[0:.1:1]); waitforbuttonpress;
end;

clf;
plot(X(:,1),X(:,2),'.',V(:,1),V(:,2),'x'); hold on;
for k=1:c,
    contour(x,y,reshape(Um(:,k),size(x)),[0:.1:1]);
end;

%[dum,idx]=max(U,[],2);
%Uhard = full(sparse(1:n,idx,1));
%Uclass = full(sparse(1:n,class,1));
%ComparingPartitions(Uhard',Uclass')
