%{
Fuzzy C-Means (FCM) demo
Course:       Computational Intelligence (Fall 2015)
Instructor:   Dr. Timothy Havens
Institution:  Michigan Technological University
Language:     MATLAB
%}

clear all;
close all;

n=50;
X=[randn(n,2)
    bsxfun(@plus,randn(n,2),[3 3])
    bsxfun(@plus,randn(n,2),[-3 3])];

c=4;

X=[-1 1
    -1 0
    -1 -1
    0 0
    1 1
    1 0
    1 -1];
c=2;


figure(1);
plot(X(:,1),X(:,2),'o');

[N,D]=size(X);

m=1.7;

init=randperm(N);
V=X(init(1:c),:);

plot(X(:,1),X(:,2),'o',V(:,1),V(:,2),'x'); waitforbuttonpress;

m2=1/(m-1);
tmax=100;
eps=1e-3;
Vold=zeros(c,D);

%figure; hold on;
t=1;
while(any(sum((V-Vold).^2,2)>eps) && t<tmax)
    t
    Vold=V;
    D=distance2(X,V); D(D<0)=0; D=D.^m2;
    U=1./bsxfun(@times,D,sum(1./D,2));
    U(isnan(U))=1;

    Um=U.^m;

    for k=1:c,
        V(k,:)=sum(bsxfun(@times,X,Um(:,k)))/sum(Um(:,k));
    end;
    plot(X(:,1),X(:,2),'o',V(:,1),V(:,2),'x'); waitforbuttonpress;

    t=t+1;
    %plot(t,V(:,1),'.');
end;
%t

D=distance2(X,V); D(D<0)=0; D=D.^m2;
U=1./bsxfun(@times,D,sum(1./D,2));
U(isnan(U))=1;

[dum,idx]=max(U,[],2);
