%{
K-Means clustering demo
Course:       Computational Intelligence (Fall 2015)
Instructor:   Dr. Timothy Havens
Institution:  Michigan Technological University
Language:     MATLAB
%}

clear all; close all;

n=50;
X=[randn(n,2)
    bsxfun(@plus,randn(n,2),[5 5])
    bsxfun(@plus,randn(n,2),[-5 5])];

c=4;

% X=[-1 1
%     -1 0
%     -1 -1
%     0 0
%     1 1
%     1 0
%     1 -1];
% c=2;

figure(1);
plot(X(:,1),X(:,2),'.');

[N,D]=size(X);


init=randperm(N); label=randi(c,1,N);
V=X(init(1:c),:);

plot(X(:,1),X(:,2),'o',V(:,1),V(:,2),'x'); waitforbuttonpress;

tmax=100;
eps=1e-3;
Vold=zeros(c,D);

t=1; last=0;
while(any(label ~= last) && t<tmax)
    t
    last=label;
    D=distance2(X,V); D(D<0)=0;
    [dum,label]=min(D');

    for k=1:c,
        V(k,:)=mean(X(k==label,:));
    end;
    plot(X(:,1),X(:,2),'o',V(:,1),V(:,2),'x'); waitforbuttonpress;
    t=t+1;
end;

for k=1:c,
    SUMD(k) = sum(D(label==k,k));
end;
