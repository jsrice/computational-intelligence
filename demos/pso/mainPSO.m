%{
Run script for PSO demo
Course:       Computational Intelligence (Fall 2015)
Instructor:   Dr. Timothy Havens
Institution:  Michigan Technological University
Language:     MATLAB
%}

close all; clear all;

fitfunc = 'fitnessPSO';

Np = 100;
Nd = 10;
B = 10;
xbounds = [-B*ones(1,Nd)
    B*ones(1,Nd)];

step= 2*B/100;
[X,Y]=meshgrid(-B:step:B,-B:step:B);
F = feval(fitfunc,[X(:) Y(:)]);
figure(1);
mesh(X,Y,reshape(F,size(X))); %return


figure;
[gb,gbF]=PSO(fitfunc,Np,Nd,xbounds);
gb
gbF
