%{
Provides several sample fitness functions for PSO demo
Course:       Computational Intelligence (Fall 2015)
Instructor:   Dr. Timothy Havens
Institution:  Michigan Technological University
Language:     MATLAB
%}

function [F]=fitnessPSO(P)

x=P(:,1); y=P(:,2);
%F = -(3*(x-8.14).^2.*(y-2.71).^2+(x-8.14).^2+(y-2.72).^2);

% parabolic [-10 10]
%F = -sum(P.^2,2);

% rastrigrin [-5.12 5.12
%F = -(10*size(P,2)+sum(P.^2-10*cos(2*pi*P),2));

% shaffer's [-10 10]
F = (0.5 - ((sin(x.^2+y.^2)).^2-0.5)./(1.0+0.001*(x.^2+y.^2)).^2);

% eggholder function [-512 512], min [512,404.2319]
%F = -(-(y+47).*sin(sqrt(abs(y+x/2+47)))-x.*sin(sqrt(abs(x-(y+47)))));
