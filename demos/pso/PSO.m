%{
Particle Swarm Optimization (PSO) demo
Course:       Computational Intelligence (Fall 2015)
Instructor:   Dr. Timothy Havens
Institution:  Michigan Technological University
Language:     MATLAB
%}

function [gbest,gbestF,lbest,lbestF]=PSO(fitfunc,Np,Nd,xbounds,lbest,lbestF,gbest,gbestF);

C1 = 1.3;
C2 = 1.3;
w = 0.7;

% initialize particles
if(nargin<5)
    P = bsxfun(@plus,bsxfun(@times,rand(Np,Nd),xbounds(2,:)-xbounds(1,:)),xbounds(1,:));
else
    P = lbest;
end;

tmax = 100;
if(nargin<5)
    lbest = P; lbestF = -Inf*ones(Np,1);
    gbest = P(1,:); gbestF = -Inf;
end;
V = zeros(Np,Nd);

h=2;
t=0; Phist = zeros(Np,Nd,h);
while(t<tmax)
    fitness = feval(fitfunc,P);
    Phist(:,:,1:end-1)=Phist(:,:,2:end);

    % set local best
    [lbestF,i] = max([lbestF fitness],[],2);
    lbest(i==2,:) = P(i==2,:);

    % set global best
    [lbestFmax,i] = max(lbestF);
    if(lbestFmax>gbestF)
        gbestF = lbestFmax;
        gbest = lbest(i,:);
    end;
    gbestFvec(t+1)=gbestF;

    % update particle location
    momentum = w*V;
    personal = C1.*rand(Np,Nd).*(lbest - P);
    social = C2.*rand(Np,Nd).*bsxfun(@minus,gbest,P);
    V = momentum + personal + social;
    P = P + V;

    % bring particles back into bounds of search space
    P = P.*(bsxfun(@le,P,xbounds(2,:)))+bsxfun(@times,xbounds(2,:),bsxfun(@gt,P,xbounds(2,:)));
    P = P.*(bsxfun(@ge,P,xbounds(1,:)))+bsxfun(@times,xbounds(1,:),bsxfun(@lt,P,xbounds(1,:)));
    Phist(:,:,end)=P;


    %plot(P(:,1),P(:,2),'.');
    %quiver(P(:,1),P(:,2),momentum(:,1),momentum(:,2)); hold all;
    %quiver(P(:,1),P(:,2),personal(:,1),personal(:,2));
    %quiver(P(:,1),P(:,2),social(:,1),social(:,2));
    quiver(P(:,1)-V(:,1),P(:,2)-V(:,2),V(:,1),V(:,2),0); hold all;
    %plot(squeeze(Phist(:,1,:))',squeeze(Phist(:,2,:))','b-'); hold on;
    plot(lbest(:,1),lbest(:,2),'rx'); plot([P(:,1)-V(:,1) lbest(:,1)]',[P(:,2)-V(:,2) lbest(:,2)]','g:');
    plot(gbest(1),gbest(2),'g*');
    axis([xbounds(1,1) xbounds(2,1) xbounds(1,2) xbounds(2,2)]); drawnow; hold off;
    %waitforbuttonpress;
    pause(.1);
    t=t+1;
end;

figure(2);
plot(gbestFvec);

